getData();

async function getData() {
    const response = await fetch('task.csv');
    const csv_data = await response.text();
    const arr = [];
    const arr1 = [];
    const arr2 = [];


    const table = csv_data.split('\n');
    table.forEach(row => {
        const columns = row.split(';');
        if (columns[0] === "Short-term investments") {
            arr.push(parseFloat(columns[17].replace(/(?!-)[^0-9.]/g, "")))
        } else if (columns[0] === "Bonds") {
            arr1.push(parseFloat(columns[17].replace(/(?!-)[^0-9.]/g, "")))
        } else if (columns[0] === "Equities") {
            arr2.push(parseFloat(columns[17].replace(/(?!-)[^0-9.]/g, "")))
        }
    })
    
 //Filtering positive numbers from array
  const getPositiveNumbers = (array) => {
      return array.filter(function(value) {
        return value > 0;
      });
    }

  const positive_arr = getPositiveNumbers(arr)


  //Filtering negative numbers from array
    const getNegativeNumbers = (array) => {
      return array.filter(function(value) {
        return value < 0;
      });
    }

  const negative_arr = getNegativeNumbers(arr)

//Find the sum

let sum_arr_positive = positive_arr.reduce((a, b) => a + b, 0)
let sum_arr_negative = negative_arr.reduce((a, b) => a + b, 0)
let sum_arr1 = arr1.reduce((a, b) => a + b, 0)
let sum_arr2 = arr2.reduce((a, b) => a + b, 0)


let loans = sum_arr_negative
const loans_abs = Math.abs(loans)
const total = sum_arr_positive + sum_arr1 + sum_arr2

const nav = total - loans_abs;
 

//Chart1
 
 // set the dimensions and margins of the graph
 var width = 500
 height = 500
 margin = 20

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_chart'
var svg = d3.select("#my_chart")
.append("svg")
 .attr("width", width)
 .attr("height", height)
.append("g")
 .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


 const data = {'Total assets': total, 'Loans': loans_abs, 'NAV': nav}

 

// set the color scale
var color = d3.scaleOrdinal()
.domain(data)
.range(d3.schemeSet2)

// Compute the position of each group on the pie:
var pie = d3.pie()
.value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data))

var arcGenerator = d3.arc()
  .innerRadius(0)
  .outerRadius(radius)

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
.selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('path')
    .attr('d', arcGenerator)
    .attr('fill', function(d){ return(color(d.data.key)) })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7)
    

svg
    .selectAll('mySlices')
    .data(data_ready)
    .enter()
    .append('text')
    .text(function(d){ return d.data.key +'  ' + (d.data.value).toFixed(2) + '  USD'})
    .attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
    .style("text-anchor", "middle")
    .style("font-size", 17)




    //Chart2
 
 // set the dimensions and margins of the graph
 var width = 500
 height = 500
 margin = 20

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_chart'
var svg = d3.select("#my_chart1")
.append("svg")
 .attr("width", width)
 .attr("height", height)
.append("g")
 .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


 const data1 = {'Short-term investments': sum_arr_positive  , 'Bonds': sum_arr1, 'Equties': sum_arr2}
 


// set the color scale
var color = d3.scaleOrdinal()
.domain(data1)
.range(d3.schemeSet2)

// Compute the position of each group on the pie:
var pie = d3.pie()
.value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data1))

var arcGenerator = d3.arc()
  .innerRadius(0)
  .outerRadius(radius)

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
.selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('path')
    .attr('d', arcGenerator)
    .attr('fill', function(d){ return(color(d.data.key)) })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7)
    

svg
    .selectAll('mySlices')
    .data(data_ready)
    .enter()
    .append('text')
    .text(function(d){ return d.data.key +'  ' + (d.data.value).toFixed(2) + '  USD'})
    .attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
    .style("text-anchor", "middle")
    .style("font-size", 17)
}









